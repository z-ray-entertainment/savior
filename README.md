# Savior

## About
Savior is a small application which aims to save you headaches by accidentally
loosing files.
Original this is designed to build your own custom save game cloud,
cross platform, cross store and also cross computer.

## Usage
You select source folders from where Savior should obtain files and folders
from and snyc them to any destination you told it.
Destination could be local folders, Samba shares or even a git repository.